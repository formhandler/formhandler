<?php
/**
 * Class file
 */

/**
 * class PercentageField
 *
 * Create a percentage field
 *
 * @author Marien den Besten
 * @package FormHandler
 * @subpackage Fields
 */
class PercentageField extends NumberField
{
    private $valueObject = false;
    
    /**
     * Constructor
     *
     * @param FormHandler $form The form where this field is located on
     * @param string $name The name of the field
     * @return PercentageField
     * @author Marien den Besten
     */
    public function __construct(FormHandler $form, $name)
    {
        return parent::__construct($form, $name)
            ->setMin(0)
            ->setExtra('class="percentage-field"')
            ->setValidator(_FH_FLOAT);
    }
    
    /**
     * Set if the returned value needs to be an object
     * 
     * @param bool $bool
     */
    public function setValueObject($bool)
    {
        $this->valueObject = (bool) $bool;
    }
    
    /**
     * Set value
     * @param TechBase\Type\Percentage|integer $value
     * @return PercentageField
     */
    public function setValue($value, $forced = false)
    {
        if(is_object($value) && get_class($value) == 'TechBase\Type\Percentage')
        {
            $value = $value->get();
        }
        return parent::setValue($value, $forced);
    }
    
    /**
     * Return the value of the field
     * @return type
     */
    public function getValue()
    {
        $value = parent::getValue();
        
        return $this->valueObject ? new \TechBase\Type\Percentage($value) : $value;
    }
    
    public function processValidators()
    {
        //formhandler can not handle objects yet
        $tmp = $this->valueObject;
        $this->valueObject = false;
        $return = parent::processValidators();
        $this->valueObject = $tmp;
        return $return;
    }

    /**
     * TextField::getField()
     *
     * Return the HTML of the field
     *
     * @return string the html
     * @access public
     * @author Teye Heimans
     */
    public function getField()
    {
        $this->setExtraAfter('%');

        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return $this->_getViewValue() .'%';
        }

        //formhandler can not handle objects yet
        $tmp = $this->valueObject;
        $this->valueObject = false;
        $return = parent::getField();
        $this->valueObject = $tmp;
        return $return;
    }
}