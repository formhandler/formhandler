<?php
/**
 * class SelectField
 *
 * Create a SelectField
 *
 * @author Teye Heimans
 * @package FormHandler
 * @subpackage Fields
 */
class SelectField extends Field
{
    protected $size;
    private $multiple;
    private $options_classes;

    /**
     * SelectField::SelectField()
     *
     * Public constructor: Create a selectfield object
     *
     * @param FormHandler $form The form where the field is located on
     * @param string $name The name of the form
     * @return SelectField
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function __construct(FormHandler $form, $name)
    {
        // call the constructor of the Field class
        return parent::__construct($form, $name)
            ->setJsSelectorValue('#' . $form->getFormName() . ' select[name="' . $name . '"]')
            ->setSize(1)
            ->useArrayKeyAsValue(FH_DEFAULT_USEARRAYKEY)
            ->setMultiple(false);
    }

    /**
     * SelectField::getValue()
     *
     * Return the value of the field
     *
     * @return mixed
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function getValue()
    {
        $value = parent::getValue();

        // are multiple selects possible?
        if($this->multiple && $value !== '')
        {
            //force string
            if(is_numeric($value))
            {
                $value = (string) $value;
            }

            if(is_string($value))
            {
                return explode(', ', $value);
            }
            if(is_array($value))
            {
                return $value;
            }
            return array();
        }

        if($value === '')
        {
            $options = $this->getOptions();

            if(count($options) > 0)
            {
                reset($options);
                $value = key($options);
            }
        }

        return $value;
    }

    /**
     * SelectField::getField()
     *
     * Public: return the HTML of the field
     *
     * @return string the html
     * @author Teye Heimans
     * @author Marien den Besten
     * @since 12-08-2008 Altered by Johan Wiegel, repaired valid html </optgroup> thanks to Roland van Wanrooy
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return $this->_getViewValue();
        }

        // multiple selected items possible?
        $aSelected = array();
        if($this->multiple && $this->getValue() !== '')
        {
            $prepare = $this->getValue();

            // when there is a value..
            if(!is_array($prepare))
            {
                // split a string like 1, 4, 6 into an array
                $prepare = explode(',', $prepare);
            }

            //force strings
            if(is_array($prepare))
            {
                $aSelected = array_map(function($value){return (string) $value;}, $prepare);
            }
        }
        elseif($this->getValue() !== '')
        {
            $aSelected = !is_array($this->getValue())
                ? array((string) $this->getValue())
                : $this->getValue();
        }

        // create the options list
        $sOptions = '';

        // added by Roland van Wanrooy: flag to indicate an optgroup, in order to close it properly
        $bOptgroup = false;
        // added by Roland van Wanrooy: string with the close tag
        $sOGclose = "\t</optgroup>\n";
        $options = $this->getOptions();

        foreach($options as $iKey => $sValue)
        {
            // use the array value as field value if wanted
            if(!$this->getUseArrayKeyAsValue())
            {
                $iKey = $sValue;
            }

            if(strpos($iKey, 'LABEL'))
            {
                // added by Roland van Wanrooy: close the optgroup if there is one
                $sOptions .= ($bOptgroup ? $sOGclose : '');
                $sOptions .= "\t<optgroup label=\"" . $sValue . "\">\n";

                // added by Roland van Wanrooy: flag opgroup as true
                $bOptgroup = true;
            }
            else
            {
                $sOptions .= sprintf(
                    "\t<option %s value=\"%s\" %s>%s</option>\n", $this->options_classes[$iKey],
                    $iKey,
                    (in_array((string) $iKey, $aSelected) ? ' selected="selected"' : ''),
                    str_replace(' ', '&nbsp;', $sValue)
                );
            }
        }

        // when no options are set, set an empty options for XHML compatibility
        if(empty($sOptions))
        {
            $sOptions = "\t<option>&nbsp;</option>\n\t";
        }
        // added by Roland van Wanrooy:
        // $sOptions is not empty, so if there was an <opgroup> then close is properly
        else
        {
            $sOptions .= ($bOptgroup ? $sOGclose : '');
        }

        // return the field
        return sprintf(
            '<select name="%s" id="%s" size="%d"%s>%s</select>%s',
            $this->name . ( $this->multiple ? '[]' : ''),
            $this->name,
            is_null($this->size) ? 1 : $this->size,
            ($this->multiple ? ' multiple="multiple"' : '' )
                . (isset($this->tab_index) ? ' tabindex="' . $this->tab_index . '" ' : '')
                . (isset($this->extra) ? ' ' . $this->extra : '' )
                . ($this->getDisabled() && !$this->getDisabledInExtra() ? 'disabled="disabled" ' : ''),
            $sOptions,
            (isset($this->extra_after) ? $this->extra_after : '')
        );
    }

    /**
     *  added by sid benachenhou for handling styles
     *
     * @param string $class_option
     * @return SelectField
     */
    public function setCOptions($class_option)
    {
        $this->options_classes = $class_option;
        return $this;
    }

    /**
     * SelectField::setMultiple()
     *
     * Set if multiple items can be selected or not
     *
     * @param boolean|null $bMultiple
     * @return SelectField
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function setMultiple($bMultiple)
    {
        if(!is_null($bMultiple))
        {
            $this->multiple = $bMultiple;
        }
        return $this;
    }

    /**
     * SelectField::setSize()
     *
     * Set the size of the field
     *
     * @param integer|null $size the new size
     * @return SelectField
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function setSize($size)
    {
        if(!is_null($size))
        {
            $this->size = $size;
        }
        return $this;
    }

    /**
     * Get the display value of the field
     *
     * @return string
     */
    public function getDisplayValue()
    {
        $value = $this->getValue();
        $options = $this->getOptions();

        return (is_scalar($value) && is_array($options) && array_key_exists($value, $options))
            ? $options[$value]
            : '';
    }
}