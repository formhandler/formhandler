<?php

/**
 * class ListField
 *
 * Create a listfield on the given form
 *
 * @author Teye Heimans
 * @package FormHandler
 * @subpackage Fields
 */
class ListField extends Field
{
    private $field_values;
    private $field_on;
    private $field_off;
    private $field_on_title;
    private $field_off_title;
    private $vertical_mode;

    /**
     * ListField::ListField()
     *
     * Constructor: Create a new ListField
     *
     * @param FormHandler $form The form where this field is located on
     * @param string $name The name of the field
     * @param array $options The options of the field
     * @return ListField
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function __construct(FormHandler $form, $name, $options = array())
    {
        $this->field_values = new HiddenField($form, $name);
        $this->field_values->setDefaultValue(array());
        static $bSetJS = false;

        // needed javascript included yet ?
        if(!$bSetJS)
        {
            $bSetJS = true;
            $form->_setJS(FH_FHTML_DIR . "js/listfield.js", true);
        }

        // make the fields of the listfield
        $this->field_on = new SelectField($form, $name . '_ListOn');
        $this->field_off = new SelectField($form, $name . '_ListOff');
        $this->field_on->setMultiple(true);
        $this->field_off->setMultiple(true);

        return parent::__construct($form, $name)
            ->setOptions($options)
            ->useArrayKeyAsValue(FH_DEFAULT_USEARRAYKEY)
            ->setSize(FH_DEFAULT_LISTFIELD_SIZE)
            ->setOffTitle($form->_text(29))
            ->setOnTitle($form->_text(30))
            ->setFocusName($name . '_ListOn');
    }

    /**
     * ListField::SetVerticalMode()
     *
     * Set the stack mode of the list field
     *
     * @param boolean $vertical_mode
     * @author Rick de Haan
     * @author Marien den Besten
     * @since 20-03-2008 added by Johan Wiegel
     * @return ListField
     */
    public function setVerticalMode($vertical_mode)
    {
        if(!is_null($vertical_mode))
        {
            $this->vertical_mode = $vertical_mode;
        }
        return $this;
    }

    /**
     * ListField::setValue()
     *
     * Set the value of the field
     *
     * @param array|string $aValue The new value of the field
     * @return ListField
     * @author Teye Heimans
     */
    public function setValue($aValue, $forced = false)
    {
        $this->field_values->setValue($aValue, $forced);
        return $this;
    }
    
    /**
     * Get the value of the field
     * 
     * @return array
     * @author Marien den Besten
     */
    public function getValue()
    {
        return $this->field_values->getValue();
    }

    /**
     * ListField::setExtra()
     *
     * Set some extra tag information of the fields
     *
     * @param string $extra The extra information to inglude with the html tag
     * @param boolean $append Append extras to already defined values
     * @return ListField
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function setExtra($extra, $append = false)
    {
        $this->field_off->setExtra($extra, $append);
        $this->field_on->setExtra($extra, $append);
        return $this;
    }

    /**
     * ListField::setOnTitle()
     *
     * Set the title of the ON selection of the field
     *
     * @param string $sTitle The title
     * @return ListField
     * @author Teye Heimans
     */
    public function setOnTitle($sTitle)
    {
        if(!is_null($sTitle))
        {
            $this->field_on_title = $sTitle;
        }
        return $this;
    }

    /**
     * ListField::setOffTitle()
     *
     * Set the title of the OFF selection of the field
     *
     * @param string $sTitle The title
     * @return ListField
     * @author Teye Heimans
     */
    public function setOffTitle($sTitle)
    {
        if(!is_null($sTitle))
        {
            $this->field_off_title = $sTitle;
        }
        return $this;
    }
    
    /**
     * Set disabled
     * 
     * @param boolean $bool
     * @return Field
     */
    public function setDisabled($bool = true)
    {
        $this->field_on->setDisabled($bool);
        $this->field_off->setDisabled($bool);
        return parent::setDisabled($bool);
    }

    /**
     * ListField::getField()
     *
     * Return the HTML of the field
     *
     * @return string: The html
     * @author Teye Heimans
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return $this->_getViewValue();
        }

        // get the selected and unselected values
        $current = !is_array($this->getValue()) ? array($this->getValue()) : $this->getValue();
        $aSelected = array();
        $aUnselected = array();
        foreach($this->getOptions() as $iIndex => $sValue)
        {
            $sKey = (!$this->getUseArrayKeyAsValue()) ? $sValue : $iIndex;

            if(in_array($sKey, $current))
            {
                $aSelected[$iIndex] = $sValue;
            }
            else
            {
                $aUnselected[$iIndex] = $sValue;
            }
        }

        $this->field_on->setOptions($aSelected);
        $this->field_off->setOptions($aUnselected);

        // add the double click event
        $this->field_on->extra .= " ondblclick=\"changeValue('" . $this->name . "', false)\"";
        $this->field_off->extra .= " ondblclick=\"changeValue('" . $this->name . "', true)\"";

        return
            $this->field_values->getField() . "\n" .
            str_replace(
                array(
                '%onlabel%',
                '%offlabel%',
                '%onfield%',
                '%offfield%',
                '%name%',
                '%ontitle%',
                '%offtitle%'
                ), array(
                $this->field_on_title,
                $this->field_off_title,
                $this->field_on->getField(),
                $this->field_off->getField(),
                $this->name,
                sprintf($this->form_object->_text(34), utils::html(strip_tags($this->field_off_title))),
                sprintf($this->form_object->_text(34), utils::html(strip_tags($this->field_on_title)))
                ), (!empty($this->vertical_mode) && $this->vertical_mode) ? FH_LISTFIELD_VERTICAL_MASK : FH_LISTFIELD_HORIZONTAL_MASK
            ) .
            (isset($this->extra_after) ? $this->extra_after : '');
    }

    /**
     * ListField::setSize()
     *
     * Set the size (height) of the field (default 4)
     *
     * @param integer $size The size
     * @return ListField
     * @author Teye Heimans
     */
    public function setSize($size)
    {
        $this->field_on->setSize($size);
        $this->field_off->setSize($size);
        return $this;
    }

    /**
     * ListField::useArrayKeyAsValue()
     *
     * Set if the array keys of the options has to be used as values for the field
     *
     * @param boolean $mode The mode
     * @return ListField
     * @author Teye Heimans
     */
    public function useArrayKeyAsValue($mode)
    {
        if(!is_null($mode))
        {
            parent::useArrayKeyAsValue($mode);
            $this->field_on->useArrayKeyAsValue($mode);
            $this->field_off->useArrayKeyAsValue($mode);
        }
        return $this;
    }
}
