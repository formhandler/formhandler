<?php

/**
 * class TemperatureField
 *
 * Create a Temperaturefield
 *
 * @author Marien den Besten
 * @package FormHandler
 * @subpackage Fields
 */
class TemperatureField extends Field
{
    private $temperature;
    private $unit;
    private $empty;
    private $preferred_unit;
    private $allow_empty = false;
    private $value_set = false;
    private $units = array(
        'celsius' => 'Celsius',
        'fahrenheit' => 'Fahrenheit'
    );
    private $valueObject = false;

    /**
     * TemperatureField::__construct()
     *
     * Constructor: Create a new temperature field object
     *
     * @param FormHandler $form The form where this field is located on
     * @param string $name The name of the field
     * @return TemperatureField
     * @author Marien den Besten
     */
    public function __construct(FormHandler $form, $name)
    {
        $this->empty = new CheckBox($form, $name .'_empty');
        $this->empty->setOptions(array(1 => 'Value unknown'));

        $empty = $this->empty->getValue();
        $this->value = !empty($empty) ? null : $this->value;

        $this->temperature = new NumberField($form, $name .'_temperature');
        $this->temperature->setMin(-1000);
        $this->temperature->setMax(1000);
        $this->temperature->setStep(0.1);
        $this->temperature->setValidator(function($value)
        {
            return is_numeric($value);
        });

        $this->unit = new SelectField($form, $name .'_unit');
        $this->unit->setOptions($this->units);

        if(empty($empty))
        {
            $this->value = array(
                $this->temperature->getValue(),
                $this->unit->getValue()
            );
        }

        //Classes
        $this->temperature->setExtra('class="temperature-field"');
        $this->unit->setExtra('class="temperature-field"');
        $this->empty->setExtra('class="temperature-field"');

        parent::__construct($form, $name)
            ->setFocusName($name .'_temperature');

        $form->_setJS(''
            . "$('#". $name ."_empty_1').on('change',function()\n"
            . "{\n"
            . " var state = !!$(this).attr('checked');\n"
            . " $('#". $name ."_temperature').attr('disabled',state);\n"
            . " $('#". $name ."_unit').attr('disabled',state);\n"
            . "});\n"
            . "$('#". $name ."_unit').on('change',function()\n"
            . "{\n"
            . " var val = $(this).val(),\n"
            . "     temp = $('#". $name ."_temperature').val();\n"
            . " if(val == 'celsius') $('#". $name ."_temperature').val(Math.round(((temp - 32) / 1.8)*10)/10);\n"
            . " if(val == 'fahrenheit') $('#". $name ."_temperature').val(Math.round(((temp * 1.8) + 32)*10)/10);\n"
            . "});\n",false,false);

        return $this;
    }

    /**
     * Set if the returned value needs to be an object
     *
     * @param bool $bool
     */
    public function setValueObject($bool)
    {
        $this->valueObject = (bool) $bool;
    }

    /**
     * Set if field is allowed to be empty/unknown
     *
     * @param boolean $bool
     * @author Marien den Besten
     * @return \TemperatureField
     */
    public function allowEmpty($bool)
    {
        $this->allow_empty = (bool) $bool;

        if((bool) $bool === false
            && is_null($this->getValue()))
        {
            $this->setValue(0);
        }
        return $this;
    }

    /**
     * Check if field is valid
     *
     * @author Marien den Besten
     * @return boolean
     */
    public function processValidators()
    {
        $this->temperature->processValidators();
        return $this;
    }

    /**
     * Get if the field is in error state
     *
     * @return boolean
     * @author Marien den Besten
     */
    public function getErrorState()
    {
        $empty_value = $this->empty->getValue();

        if($this->allow_empty === true
            && !empty($empty_value))
        {
            return false;
        }

        return $this->temperature->getErrorState();
    }

    /**
     * The error message
     *
     * @return string
     * @author Marien den Besten
     */
    public function getErrorMessage()
    {
        return $this->temperature->getErrorMessage();
    }

    /**
     * Set the validator
     *
     * @author Marien den Besten
     * @param string|array|callable $validator
     * @return \TemperatureField
     */
    public function setValidator($validator)
    {
        $this->temperature->setValidator($validator);
        return $this;
    }

    /**
     * Get validator
     *
     * @author Marien den Besten
     * @return string|array|callable
     */
    public function getValidators()
    {
        return $this->temperature->getValidators();
    }

    /**
     * TemperatureField::setExtra()
     *
     * Function not implemented!
     *
     * @param string $sExtra The extra information to include with the html tag
     * @return void
     * @throws Exception
     * @author Marien den Besten
     */
    public function setExtra($sExtra,$append = false)
    {
    	throw new Exception('This function has not been implemented for the temperature field');
    }

    /**
     * TemperatureField::setValue()
     *
     * Set the value of the field
     *
     * @param integer|array $sValue The new value of the field
     * @return TemperatureField
     * @author Marien den Besten
     */
    public function setValue($sValue, $forced = false)
    {
        $this->empty->setValue(0, $forced);

        if(is_object($sValue) && get_class($sValue) == 'TechBase\Type\Temperature')
        {
            $unit = $sValue->getUnit() == \TechBase\Type\Temperature::UNIT_CELSIUS ? 'celsius' : 'fahrenheit';
            parent::setValue(array($sValue->getTemperature(), $unit), $forced);
            $this->temperature->setValue($sValue->getTemperature(), $forced);
            $this->unit->setValue($unit, $forced);
        }
        elseif(is_array($sValue) && count($sValue) == 2)
        {
            parent::setValue($sValue, $forced);
            $this->temperature->setValue($sValue[0], $forced);
            $this->unit->setValue($sValue[1], $forced);
        }
        elseif(is_numeric($sValue))
        {
            $this->temperature->setValue($sValue, $forced);
            parent::setValue(array($sValue,$this->unit->getValue()), $forced);
        }
        elseif(is_null($sValue))
        {
            $this->empty->setValue(1, $forced);
            parent::setValue(null, $forced);
        }

        $current_unit = $this->unit->getValue();
        $preferred_unit = $this->preferred_unit;

        if(!is_null($sValue)
            && !is_null($preferred_unit)
            && $current_unit != $preferred_unit)
        {
            $value = $this->convert(
                $this->temperature->getValue(),
                $current_unit,
                $preferred_unit
            );
            $this->temperature->setValue($value, $forced);
            $this->unit->setValue($preferred_unit, $forced);
            parent::setValue(array($value,$preferred_unit), $forced);
        }
        $this->value_set = true;
        return $this;
    }

    /**
     * TemperatureField::getValue()
     *
     * Return the current value of the field
     *
     * @return array the value of the field
     * @author Marien den Besten
     */
    public function getValue()
    {
        $empty = $this->empty->getValue();

        $object_unit = $this->unit->getValue() === 'fahrenheit'
        ? \TechBase\Type\Temperature::UNIT_FAHRENHEIT
        : \TechBase\Type\Temperature::UNIT_CELSIUS;

        return !empty($empty)
            ? null
            : ($this->valueObject
                ? new TechBase\Type\Temperature(
                    $this->temperature->getValue(),
                    $object_unit
                )
                : array(
                    $this->temperature->getValue(),
                    $this->unit->getValue()
                )
            );
    }

    /**
     * Set preferred unit
     *
     * @param string $unit
     * @author Marien den Besten
     */
    public function setUnit($unit)
    {
        //translate buggy definitions
        $unit = ($unit === 'celcius') ? 'celsius' : $unit;

        if(array_key_exists($unit,$this->units)
            && !$this->form_object->isPosted()) //skip preference when posted
        {
            $current = $this->unit->getValue();
            $this->unit->setValue($unit);
            $this->preferred_unit = $unit;

            if($this->getValue() === ''
                || $current == $unit)
            {
                return $this;
            }

            if($this->value_set)
            {
                $value = $this->convert($this->temperature->getValue(), $current, $unit);

                $this->value = array($value,$unit);
                $this->temperature->setValue($value);
            }
        }
        return $this;
    }

    /**
     * Convert between formats
     *
     * @author Marien den Besten
     * @param integer $value
     * @param string $from
     * @param string $to
     * @return type
     */
    private function convert($value,$from,$to)
    {
        if($from == 'fahrenheit')
        {
            return round(($value - 32) / 1.8,1);
        }
        if($from == 'celsius' || $from == 'celcius')
        {
            return round(($value * 1.8) + 32,1);
        }
    }

    /**
     * Get view value
     *
     * @author Marien den Besten
     * @return string
     */
    public function _getViewValue()
    {
        return (is_null($this->getValue()))
            ? '-'
            : $this->temperature->getValue() .'&deg; '. $this->units[$this->unit->getValue()];
    }

    /**
     * Set disabled
     *
     * @param boolean $bool
     * @return Field
     */
    public function setDisabled($bool = true)
    {
        $this->empty->setDisabled($bool);
        $this->unit->setDisabled($bool);
        $this->temperature->setDisabled($bool);
        return parent::setDisabled($bool);
    }

    /**
     * TemperatureField::getField()
     *
     * Return the HTML of the field
     *
     * @return string the html
     * @author Marien den Besten
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return $this->_getViewValue();
        }

        if(is_null($this->getValue()))
        {
            $this->empty->setValue(1);
            $this->unit->setDisabled();
            $this->temperature->setDisabled();
        }

        return
            $this->temperature->getField() .'&deg; '. $this->unit->getField()
            . ($this->allow_empty === true
                ? '<div class="temperature-field-unknown">'. $this->empty->getField() .'</div>'
                : '');
    }
}