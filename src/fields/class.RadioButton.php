<?php

/**
 * class RadioButton
 *
 * Create a RadioButton
 *
 * @author Teye Heimans
 * @package FormHandler
 * @subpackage Fields
 */
class RadioButton extends Field
{
    // string: what kind of "glue" should be used to merge the fields
    private $mask;
    // object: a maskloader object
    private $loader;

    /**
     * RadioButton::RadioButton()
     *
     * Constructor: Create a new radiobutton object
     *
     * @param object $form The form where this field is located on
     * @param string $name The name of the field
     * @param array|string $options The options for the field
     * @return RadioButton
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function __construct(FormHandler $form, $name, $options = array())
    {
        // call the constructor of the Field class
        return parent::__construct($form, $name)
            ->setOptions($options)
            ->setMask(FH_DEFAULT_GLUE_MASK)
            ->useArrayKeyAsValue(FH_DEFAULT_USEARRAYKEY)
            ->setFocusName($name . '_1');
    }

    /**
     * RadioButton::setMask()
     *
     * Set the "glue" used to glue multiple radiobuttons
     *
     * @param string $sMask
     * @return RadioButton
     * @author Teye Heimans
     * @author Marien den Besten
     * @access public
     */
    public function setMask($sMask)
    {
        if(!is_null($sMask))
        {
            // when there is no %field% used, put it in front of the mask/glue
            if(strpos($sMask, '%field%') === false)
            {
                $sMask = '%field%' . $sMask;
            }

            $this->mask = $sMask;
        }
        return $this;
    }

    /**
     * RadioButton::getField()
     *
     * Return the HTML of the field
     *
     * @return string the html of the field
     * @access Public
     * @author Teye Heimans
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return $this->_getViewValue();
        }

        $options = $this->getOptions();
        if(count($options) > 0)
        {
            $result = '';
            foreach($options as $key => $value)
            {
                if(!$this->getUseArrayKeyAsValue())
                {
                    $key = $value;
                }

                $result .= $this->getRadioButton($key, $value, true);
            }
        }
        elseif(count($options) === 0)
        {
            $result = ' ';
        }
        else
        {
            $result = $this->getRadioButton($options, '');
        }

        // when we still got nothing, the mask is not filled yet.
        // get the mask anyway
        if(empty($result))
        {
            $result = $this->loader->fill();
        }

        return $result . (isset($this->extra_after) ? $this->extra_after : '');
    }

    /**
     * RadioButton::_getRadioButton()
     *
     * Return the radiobutton with the given title and value
     *
     * @param string $value the value for the checkbox
     * @param string $title the title for the checkbox
     * @param bool $use_mask Do we need to use the mask ?
     * @return string the HTML for the checkbox
     * @author Teye Heimans
     */
    private function getRadioButton($value, $title, $use_mask = false)
    {
        static $counter = 1;

        $value = trim($value);
        $title = trim($title);

        if(is_null($this->loader))
        {
            $this->loader = new MaskLoader();
            $this->loader->setMask($this->mask);
            $this->loader->setSearch('/%field%/');
        }

        $field = sprintf(
            '<input type="radio" name="%s" id="%1$s_%d" value="%s" %s' . FH_XHTML_CLOSE . '>'
                . '<label for="%1$s_%2$d" class="noStyle">%s</label>',
            $this->name,
            $counter++,
            htmlspecialchars($value),
            ($value == $this->getValue() ? 'checked="checked" ' : '')
                . (isset($this->tab_index) ? 'tabindex="' . $this->tab_index . '" ' : '')
                . (!empty($this->extra) ? $this->extra . ' ' : '')
                . ($this->getDisabled() && !$this->getDisabledInExtra() ? 'disabled="disabled" ' : ''),
            $title
        );

        // do we have to use the mask ?
        if($use_mask)
        {
            $field = $this->loader->fill($field);
        }
        return $field;
    }
}