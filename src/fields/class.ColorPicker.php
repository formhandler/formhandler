<?php

/**
 * class ColorPicker
 *
 * Allows the user to pick a color
 *
 * @author Rick den Haan
 * @package FormHandler
 * @subpackage Fields
 * @since 02-07-2008
 */
class ColorPicker extends TextField
{
    var $sTitleAdd = "";

    /**
     * ColorPicker::ColorPicker()
     *
     * Constructor: Create a new ColorPicker object
     *
     * @param object $form The form where this field is located on
     * @param string $name The name of the field
     * @return ColorPicker
     * @author Rick den Haan
     */
    public function __construct($form, $name)
    {
        parent::__construct($form, $name);

        static $bSetJS = false;

        // needed javascript included yet ?
        if(!$bSetJS)
        {
            // include the needed javascript
            $bSetJS = true;
            $form->_setJS(FH_FHTML_DIR . "js/jscolor/jscolor.js", true);
        }
        return $this;
    }

    /**
     * ColorPicker::getField()
     *
     * Return the HTML of the field
     *
     * @return string: the html of the field
     * @author Rick den Haan
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return $this->_getViewValue();
        }

        // check if the user set a class
        if(isset($this->extra) && preg_match("/class *= *('|\")(.*)$/i", $this->extra))
        {
            // put the function into a onchange tag if set
            $this->extra = preg_replace("/class *= *('|\")(.*)$/i", "class=\"color \\2", $this->extra);
        }
        else
        {
            $this->extra = "class=\"color\"" . (isset($this->extra) ? $this->extra : '');
        }

        return sprintf(
            '<input type="text" name="%s" id="%1$s" value="%s" size="%d" %s' . FH_XHTML_CLOSE . '>%s',
            $this->name,
            htmlspecialchars($this->getValue()),
            $this->getSize(),
            (!empty($this->getMaxLength()) ? 'maxlength="' . $this->getMaxLength() . '" ' : '')
                . (isset($this->tab_index) ? 'tabindex="' . $this->tab_index . '" ' : '')
                . (isset($this->extra) ? ' ' . $this->extra . ' ' : ''),
            (isset($this->extra_after) ? $this->extra_after : '')
        );
    }
}