<?php

/**
 * class UploadFieldPlain
 *
 * File uploads handler class
 *
 * @author Marien den Besten
 * @package FormHandler
 * @subpackage Fields
 */
class UploadFieldPlain extends Field
{
    private $is_required = false;

    /**
     * UploadFieldPlain::UploadFieldPlain()
     *
     * Constructor: Create a new textfield object
     *
     * @param object $form The form where this field is located on
     * @param string $name The name of the field
     * @return UploadFieldPlain
     * @author Marien den Besten
     */
    public function __construct(FormHandler $form, $name)
    {
        // call the constructor of the Field class
        parent::__construct($form, $name);

        if($form->isPosted()
            && array_key_exists($name, $_FILES))
        {
            $this->value = $_FILES[$name];
        }

        $this->form_object->setEncoding(FormHandler::ENCODING_MULTIPART);

        return $this;
    }

    /**
     * Is required
     *
     * @param boolean $required
     * @return UploadFieldPlain
     * @author Marien den Besten
     */
    public function setRequired($required)
    {
        $this->is_required = (bool) $required;
        return $this;
    }

    /**
     * Is field valid
     *
     * @return UploadFieldPlain
     * @author Marien den Besten
     */
    public function processValidators()
    {
        $this->setErrorState(false);
        // when no uploadfield was submitted (on multi-paged forms)
        if(!isset($_FILES[$this->name]))
        {
            return $this;
        }

        // is a own error handler used?
        if(count($this->validators) != 0)
        {
            parent::processValidators();
            return $this;
        }

        // easy name to work with (this is the $_FILES['xxx'] array )
        $file = $this->value;

        if($this->is_required === true
            && (!is_array($file)
                || trim($file['name']) == ''))
        {
            //no file uploaded
            $this->setErrorMessage($this->form_object->_text(22));
            $this->setErrorState(true);
        }
        return $this;
    }

    /**
     * UploadFieldPlain::getField()
     *
     * Return the HTML of the field
     *
     * @return string the html
     * @author Marien den Besten
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return (is_array($this->value) && array_key_exists('name')) ? $this->value['name'] : '';
        }

        return sprintf(
            '<input type="file" name="%s" id="%1$s" %s'. FH_XHTML_CLOSE .'>%s',
            $this->name,
            (isset($this->tab_index) ? 'tabindex="' . $this->tab_index . '" ' : '')
                . (isset($this->extra) ? ' ' . $this->extra . ' ' : '')
                . ($this->getDisabled() && !$this->getDisabledInExtra() ? 'disabled="disabled" ' : ''),
            (isset($this->extra_after) ? $this->extra_after : '')
        );
    }

    /**
     * Set which file types are accepted
     *
     * @param string|array $format
     * @return UploadFieldPlain
     * @author Marien den Besten
     */
    public function setFileType($format)
    {
        if(is_array($format) && count($format) == 0)
        {
            return $this;
        }

        $this->extra .= ' accept="'. ((is_array($format))
            ? implode(', ', $format)
            : $format) .'"';

        return $this;
    }
}