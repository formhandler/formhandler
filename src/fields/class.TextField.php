<?php

/**
 * class TextField
 *
 * Create a textfield
 *
 * @author Teye Heimans
 * @package FormHandler
 * @subpackage Fields
 */
class TextField extends Field
{
    private $size = null;
    private $max_length = null;

    /**
     * TextField::TextField()
     *
     * Constructor: Create a new textfield object
     *
     * @param FormHandler $form The form where this field is located on
     * @param string $name The name of the field
     * @return TextField
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function __construct(FormHandler $form, $name)
    {
        // call the constructor of the Field class
        return parent::__construct($form, $name)
            ->setSize(20)
            ->setMaxlength(0);
    }

    /**
     * TextField::setSize()
     *
     * Set the new size of the field
     *
     * @param integer|null $size the new size
     * @author Teye Heimans
     * @author Marien den Besten
     * @return TextField
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * Get size
     *
     * @author Marien den Besten
     * @return integer|null
     */
    public function getSize()
    {
        return empty($this->size) ? 20 : $this->size;
    }

    /**
     * TextField::checkMaxLength()
     *
     * Check the maxlength of the field
     *
     * @param integer $iLength the maxlength
     * @return boolean
     * @author Johan Wiegel
     * @since 17-04-2009
     */
    public function checkMaxLength($iLength)
    {
        if(strlen($this->getValue()) > $iLength)
        {
            $this->error = $this->form_object->_text(14);
            return false;
        }
        return true;
    }

    /**
     * TextField::checkMinLength()
     *
     * Check the minlength of the field
     *
     * @param integer $iLength the maxlength
     * @return boolean
     * @author Johan Wiegel
     * @since 17-04-2009
     */
    public function checkMinLength($iLength)
    {
        if(strlen($this->getValue()) < $iLength)
        {
            $this->error = $this->form_object->_text(14);
            return false;
        }
        return true;
    }

    /**
     * TextField::setMaxlength()
     *
     * Set the new maxlength of the field
     *
     * @param integer $max_length the new maxlength
     * @author Teye Heimans
     * @author Marien den Besten
     * @return TextField
     */
    public function setMaxlength($max_length)
    {
        if((int) $max_length > 0)
        {
            $this->max_length = (int) $max_length;
        }
        return $this;
    }

    /**
     * Get max length of field
     *
     * @author Marien den Besten
     * @return integer|null
     */
    public function getMaxLength()
    {
        return $this->max_length;
    }

    /**
     * TextField::getField()
     *
     * Return the HTML of the field
     *
     * @return string the html
     * @author Teye Heimans
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return $this->_getViewValue();
        }

        return sprintf(
                '<input type="text" name="%s" id="%1$s" value="%s" size="%d" %s' . FH_XHTML_CLOSE . '>%s',
                $this->name,
                htmlspecialchars($this->getValue()),
                $this->getSize(),
                (!is_null($this->getMaxLength()) ? 'maxlength="' . $this->getMaxLength() . '" ' : '') .
                    (isset($this->tab_index) ? 'tabindex="' . $this->tab_index . '" ' : '') .
                    (isset($this->extra) ? ' ' . $this->extra . ' ' : '')
                    . ($this->getDisabled() && !$this->getDisabledInExtra() ? 'disabled="disabled" ' : ''),
                (isset($this->extra_after) ? $this->extra_after : '')
        );
    }
}