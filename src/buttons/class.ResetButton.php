<?php

/**
 * class ResetButton
 *
 * Create a resetbutton on the given form
 *
 * @author Teye Heimans
 * @package FormHandler
 * @subpackage Buttons
 */
class ResetButton extends Button
{
    /**
     * ResetButton::ResetButton()
     *
     * constructor: Create a new reset button object
     *
     * @param object $form the form where the button is located on
     * @param string $name the name of the button
     * @return ResetButton
     * @author Teye Heimans
     * @author Marien den Besten
     */
    public function __construct($form, $name)
    {
        return parent::__construct($form, $name)
            ->setType(self::TYPE_RESET)
            ->setCaption($form->_text(27));
    }
}