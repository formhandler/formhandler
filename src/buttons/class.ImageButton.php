<?php

/**
 * class ImageButton
 *
 * Create a image button on the given form
 *
 * @author Teye Heimans
 * @package FormHandler
 * @subpackage Buttons
 */
class ImageButton extends Button
{
    private $image;

    /**
     * ImageButton::ImageButton()
     *
     * Constructor: Create a new ImageButton object
     *
     * @param FormHandler $form the form where the image button is located on
     * @param string $name the name of the button
     * @param string $image the image we have to use as button
     * @return ImageButton
     * @author Teye Heimans
     */
    public function __construct(FormHandler $form, $name)
    {
        return parent::__construct($form, $name);
    }

    /**
     * ImageButton::setImage();
     *
     * Set the image location to be used for the button
     *
     * @param string $image
     * @return ImageButton
     * @author Marien den Besten
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * ImageButton::getButton()
     *
     * Return the HTML of the button
     *
     * @return string the HTML of the button
     * @author Teye Heimans
     */
    public function getButton()
    {
        // return the button
        return sprintf(
          '<input type="image" src="%s" name="%s" id="%2$s"%s '. FH_XHTML_CLOSE .'>',
          $this->image,
          $this->name,
          (isset($this->extra) ? ' '.$this->extra:'').
          (isset($this->tab_index) ? ' tabindex="'.$this->tab_index.'"' : '')
        );
    }
}