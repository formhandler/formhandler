<?php

/**
 * class SubmitButton
 *
 * Create a submitbutton on the given form
 *
 * @author Teye Heimans
 * @package FormHandler
 * @subpackage Buttons
 */
class SubmitButton extends Button
{
    /**
     * Constructor: The constructor to create a new Submitbutton object.
     *
     * @param object $form the form where this field is located on
     * @param string $name the name of the button
     * @return SubmitButton
     * @author Teye Heimans
     */
    public function __construct($form, $name)
    {
        return parent::__construct($form, $name)
            ->setType(self::TYPE_SUBMIT)
            ->setCaption($form->_text(26));
    }
}