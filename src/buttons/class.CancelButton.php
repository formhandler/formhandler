<?php

/**
 * class CancelButton
 *
 * Create a cancel button on the given form
 *
 * @author Marien den Besten
 * @package FormHandler
 * @subpackage Buttons
 */
class CancelButton extends Button
{
    private $url = null;

    /**
     * Constructor: Create a new ImageButton object
     *
     * @param FormHandler $form the form where the image button is located on
     * @param string $name the name of the button
     * @return CancelButton
     * @author Marien den Besten
     */
    public function __construct(FormHandler $form, $name)
    {
        return parent::__construct($form, $name);
    }

    /**
     * Set the url location for button click
     *
     * @author Marien den Besten
     * @param string $url
     * @return CancelButton
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Get the button
     *
     * @author Marien den Besten
     * @return string
     */
    public function getButton()
    {
        $this->extra .= ' onclick="';
        $this->extra .= (is_null($this->url))
            ? 'history.back(-1)'
            : 'document.location.href=\''.$this->url.'\'';
        $this->extra .= '"';

        if(is_null($this->caption))
        {
            $this->caption = $this->form->_text(28);
        }

        return parent::getButton();
    }
}