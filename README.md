# README #

This project is a fork of the FormHandler project at www.formhandler.net 

The project slowly deviates from the original library. Mainly focussed on PHP5 and dynamicly linking of fields. Dropped support for database and complex fields.

It is maintained by the ColorBase team located in the Netherlands. If you have any questions, or want help by using the project, feel free to contact us.

support@color-base.com