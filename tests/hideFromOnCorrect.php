<?php

/*
 * Document
 *
 * @author marien
 * @copyright ColorBase™
 */

ob_start();

include '../src/FormHandler.php';

$form = new FormHandler();

for($i = 1; $i <= 15; $i++)
{
    TextField::set($form, 'Field '. $i, 'field_'. $i);
}

$form->getField('field_1')->hideFromOnCorrect();
$form->getField('field_3')->hideFromOnCorrect();
$form->getField('field_5')->hideFromOnCorrect();
$form->getField('field_7')->hideFromOnCorrect();

$form->onCorrect(function($data)
{
    echo '<pre>Field 1, 3, 5, 7 should be hidden from results';
    print_r($data);
    echo '</pre>';
    exit;
});

SubmitButton::set($form, 'Press submit to see if the oncorrect data works');

$form->flush();