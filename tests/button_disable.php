<?php

/**
 * @author Ruben de Vos <ruben@color-base.com>
 * @copyright ColorBase™
 */

include '../src/FormHandler.php';

$form = new FormHandler();

Button::set($form, 'Disable Button')
    ->setDisabled();

Button::set($form, 'Disable and enable Button')
    ->setDisabled()
    ->setDisabled(false);

CancelButton::set($form, 'Disable CancelButton')
    ->setDisabled();

ResetButton::set($form, 'Disable ResetButton')
    ->setDisabled();

$var = $form->flush(true);

echo 'Test for button confirmation';

echo '<hr><script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>';

echo $var;