<?php

/*
 * Document
 *
 * @author marien
 * @copyright ColorBase™
 */

ob_start();

include '../src/FormHandler.php';

$form = new FormHandler();

$form->_setJS('http://code.jquery.com/jquery-1.11.1.min.js', true);

for($i = 1; $i <= 15; $i++)
{
    TextField::set($form, 'Field '. $i, 'field_'. $i)
        ->setDefaultValue('Value '. $i);
}

$form->link('field_1', 'field_2', function()
{
    return FormHandler::returnDynamic(null, null, true, null, null);
});

$form->getField('field_10')->setDisabled();

$form->flush();