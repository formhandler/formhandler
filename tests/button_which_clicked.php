<?php

/**
 * @author Ruben de Vos <ruben@color-base.com>
 * @copyright ColorBase™
 */

include '../src/FormHandler.php';

$form = new FormHandler();

$form->addLine('Button with confirmation on click', true);
SubmitButton::set($form, 'Button 1', 'btn_1');
SubmitButton::set($form, 'Button 2', 'btn_2');
SubmitButton::set($form, 'Button 3', 'btn_3');

$form->onCorrect(function($data, FormHandler $form)
{
    return 'Button "'. $form->getButtonClicked() . '" clicked';
});

$var = $form->flush(true);

echo 'Test for reading out which button has been click in the onCorrect';

echo '<hr><script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>';

echo $var;