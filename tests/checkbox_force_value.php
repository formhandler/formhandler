<?php

/*
 * Document
 *
 * @author marien
 * @copyright ColorBase™
 */


include '../src/FormHandler.php';

$form = new FormHandler();

CheckBox::set($form, '', 'inherit')
    ->setOptions(array(
        1 => 'Normal option',
        2 => 'Force value',
        3 => 'Normal option 2',
        4 => 'Default value option',
    ))
    ->setValue(1)
    ->setValue(2, true, true)
    ->setValue(3, false, true)
    ->setDefaultValue(4);

SubmitButton::set($form, 'Submit');

$form->onCorrect(function(){ return false; });

$var = $form->flush(true);

echo 'Test for forcing values on checkboxes';

echo '<hr><script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>';

echo $var;