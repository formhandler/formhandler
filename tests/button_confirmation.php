<?php

/**
 * @author Ruben de Vos <ruben@color-base.com>
 * @copyright ColorBase™
 */

include '../src/FormHandler.php';

$form = new FormHandler();

$form->addLine('Button with confirmation on click', true);
Button::set($form, 'Click me!', 'btn_1')
    ->setConfirmation('Do you really want to click this button?');

$form->addLine('<br>Button with confirmation and description on click', true);
Button::set($form, 'Click me!', 'btn_2')
    ->setConfirmation('Do you really want to click this button?')
    ->setConfirmationDescription('And some extra description');

$form->addLine('<br>Button with confirmation, description and alert after confirmation approved', true);
Button::set($form, 'Click me!', 'btn_3')
    ->setConfirmation('Do you really want to click this button?')
    ->setConfirmationDescription('And some extra description')
    ->setExtra('onclick="alert(\'This alert should be displayed after confirmation is success\');"');

$var = $form->flush(true);

echo 'Test for button confirmation';

echo '<hr><script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>';

echo $var;