<?php

/* 
 * Document
 *  
 * @author marien
 * @copyright ColorBase™
 */

include '../src/FormHandler.php';

echo 'Test for multi link view mode';

echo '<hr>';

$form = new FormHandler();

TextField::set($form, 'Single value', 'field1')
    ->setViewMode()
    ->setValue('VALUE')
    ->setViewModeLink('startofurl?value={$value}&extra=1');

SelectField::set($form, 'Multi value', 'field2')
    ->setViewMode()
    ->setValue(array(1,2))
    ->setOptions(array(1 => 'Value 1', 2 => 'Value 2', 3 => 'Value 3'))
    ->setViewModeLink('startofurl?value={$value}&extra=1');

$form->flush();