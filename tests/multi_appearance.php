<?php

/* 
 * Document
 *  
 * @author marien
 * @copyright ColorBase™
 */

include '../src/FormHandler.php';

$form = new FormHandler();

$form->_setJS('http://code.jquery.com/jquery-1.11.1.min.js', true);

SelectField::set($form, 'Multi value 1', 'watch1')
    ->setOptions(array(1 => 'Value 1', 2 => 'Value 2', 3 => 'Value 3'));

SelectField::set($form, 'Multi value 2', 'watch2')
    ->setOptions(array(1 => 'Value 1', 2 => 'Value 2', 3 => 'Value 3'));

for($i = 1; $i <= 20; $i++)
{
    TextField::set($form, 'Field '. $i, 'field_'. $i)
        ->setAppearanceCondition(array('watch1','watch2'), function($value)
        {
            return $value['watch2'] == 2;
        });
}

$form->flush();

echo '<hr>';

echo 'Test for grouping multi appearance. Setting multi value 2 on value 2 should display all profiles, with one db request';