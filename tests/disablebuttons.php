<?php

/*
 * Document
 *
 * @author marien
 * @copyright ColorBase™
 */


include '../src/FormHandler.php';

fh_conf('FH_DEFAULT_DISABLE_SUBMIT_BTN', true);

$form = new FormHandler();

$form->_setJS('http://code.jquery.com/jquery-1.11.1.min.js', true);

for($i = 1; $i <= 15; $i++)
{
    TextField::set($form, 'Field '. $i, 'field_'. $i)
        ->setDefaultValue('Value '. $i);
}

SubmitButton::set($form, 'Submit 1')
    ->onClick(function()
    {
        echo 'Submit 1 clicked';
    });
SubmitButton::set($form, 'Submit 2')
    ->onClick(function()
    {
        echo 'Submit 2 clicked';
    });

$f = $form->flush(true);
echo '<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>' . $f;